// Copyright (C) 2017, 2023, 2024 Alexander Necheff
// This program is licensed under the terms of the LGPLv3.
// See the COPYING and COPYING.LESSER files that came packaged with this source code for the full terms.

#include <stdlib.h>

#include <alibc/vector.h>
#include <alibc/queue.h>
#include <alibc/rbtree.h>
#include <alibc/stack.h>

enum {RED, BLACK};

struct RbNode {
    void *data;
    int8_t color;
    RbNode *left;
    RbNode *right;
    RbNode *parent;
};

/*
 * Return a pointer to an RbNode holding the specified data as a child of the specified RbNode, or NULL on error.
 */
static RbNode *rbnode_alloc(void *, RbNode *);

/*
 * Frees RbNodes created by rbnode_alloc().
 */
static void rbnode_free(RbNode *);

/*
 * Returns true if the specified RbNode is a leaf, otherwise false.
 */
static int8_t rbnode_isaleaf(RbNode *);

/*
 * Returns true if the specified RbNode is red, otherwise false.
 */
static int8_t rbnode_isred(RbNode *);

/*
 * Recursive part of rbtree_insert().
 */
static void *rbtree_insert_rec(RbTree *, RbNode *, void *);

/*
 * Rotates the specified RbNode left in the specified RbTree.
 */
static void rbtree_rotate_left(RbTree *, RbNode *);

/*
 * Rotates the specified RbNode right in the specified RbTree.
 */
static void rbtree_rotate_right(RbTree *, RbNode *);

/*
 * Recursive part of rbtree_depth_first().
 */
static void rbtree_depth_first_rec(RbNode *, Vector *);

/*
 * Recursive part of rbtree_breadth_first().
 */
static void rbtree_breadth_first_rec(Queue *, Vector *);

/*
 * Rebalances the RbTree after inserting the node.
 */
static void rbtree_balance(RbTree *, RbNode *);

static uint64_t rbtree_hight_rec(RbNode *);

static RbNode *rbnode_alloc(void *data, RbNode *p)
{
    RbNode *n = calloc(1, sizeof(*n));
    n->data = data;
    n->parent = p;
    // nodes are always inserted into
    // the tree as RED before balance
    n->color = RED;
    n->left = NULL;
    n->right = NULL;

    return n;
}

static void rbnode_free(RbNode *n)
{
    free(n);
}

static int8_t rbnode_isaleaf(RbNode *n)
{
    return (n->left == NULL &&
            n->right == NULL);
}

RbTree *rbtree_alloc(int32_t (*comp)(void *,void *))
{
    RbTree *t = calloc(1, sizeof(*t));
    t->size = 0;
    t->root = NULL;
    t->comp = comp;

    return t;
}

void rbtree_free(RbTree *t)
{
    free(t);
}

void *rbtree_insert(RbTree *t, void *data)
{
    RbNode *n = NULL;
    void *val = NULL;

    if (!t || !data) {
        return NULL;
    }

    if (!t->root) {
        // insert case 1 - the root is always black
        n = rbnode_alloc(data, NULL);
        n->color = BLACK;
        t->root =n;
        t->size++;
        return val;
    }

    int64_t res = t->comp(t->root->data, data);
    if (res > 0) {
        if (t->root->left) {
            val = rbtree_insert_rec(t, t->root->left, data);
        } else {
            n = rbnode_alloc(data, t->root);
            t->root->left = n;
            t->size++;
        }
    } else if (res < 0) {
        if (t->root->right) {
            val = rbtree_insert_rec(t, t->root->right, data);
        } else {
            n = rbnode_alloc(data, t->root);
            t->root->right = n;
            t->size++;
        }
    } else {
        val = t->root->data;
        t->root->data = data;
    }

    return val;
}

static void *rbtree_insert_rec(RbTree *t, RbNode *cur, void *data)
{
    void *val = NULL;
    RbNode *n = NULL;
    int64_t res = t->comp(cur->data, data);

    if (res > 0) {
        if (cur->left) {
            val = rbtree_insert_rec(t, cur->left, data);
        } else {
            n = rbnode_alloc(data, cur);
            cur->left = n;
            t->size++;
            rbtree_balance(t, n);
        }
    } else if (res < 0) {
        if (cur->right) {
            val = rbtree_insert_rec(t, cur->right, data);
        } else {
            n = rbnode_alloc(data, cur);
            cur->right = n;
            t->size++;
            rbtree_balance(t, n);
        }
    } else {
        val = cur->data;
        cur->data = data;
    }

    return val;
}

static void rbtree_balance(RbTree *t, RbNode *node)
{
    RbNode *n = node;

    while (rbnode_isred(n->parent) && n != t->root) {
        if (n->parent == n->parent->parent->left) {
            RbNode *u = n->parent->parent->right;
            if (rbnode_isred(u)) {
                // case 1
                n->parent->color = BLACK;
                u->color = BLACK;
                n->parent->parent->color = RED;
                n = n->parent->parent;
            } else {
                if (n == n->parent->right) {
                    // case 2
                    n = n->parent;
                    rbtree_rotate_left(t, n);
                }
                // case 3
                n->parent->color = BLACK;
                n->parent->parent->color = RED;
                rbtree_rotate_right(t, n->parent->parent);
            }
        } else {
            RbNode *u = n->parent->parent->left;
            if (rbnode_isred(u)) {
                //case 1
                n->parent->color = BLACK;
                u->color = BLACK;
                n->parent->parent->color = RED;
                n = n->parent->parent;
            } else {
                if (n == n->parent->left) {
                    // case 2
                    n = n->parent;
                    rbtree_rotate_right(t, n);
                }
                // case 3
                n->parent->color = BLACK;
                n->parent->parent->color = RED;
                rbtree_rotate_left(t, n->parent->parent);
            }
        }
    }

    t->root->color = BLACK;
}

static int8_t rbnode_isred(RbNode *n)
{
    if (!n) {
        return 0;
    }

    return (n->color == RED);
}

static void rbtree_rotate_left(RbTree *t, RbNode *n)
{
    RbNode *sibling = n->right;
    n->right = sibling->left;

    if (sibling->left) {
        sibling->left->parent = n;
    }
    sibling->parent = n->parent;
    if (n->parent) {
        if (n == n->parent->left) {
            n->parent->left = sibling;
        } else {
            n->parent->right = sibling;
        }
    } else {
        t->root = sibling;
    }
    sibling->left = n;
    n->parent = sibling;
}

static void rbtree_rotate_right(RbTree *t, RbNode *n)
{
    RbNode *sibling = n->left;
    n->left = sibling->right;

    if (sibling->right) {
        sibling->right->parent = n;
    }
    sibling->parent = n->parent;
    if (n->parent) {
        if (n == n->parent->right) {
            n->parent->right = sibling;
        } else {
            n->parent->left = sibling;
        }
    } else {
        t->root = sibling;
    }
    sibling->right = n;
    n->parent = sibling;
}

uint64_t rbtree_hight(RbTree *t)
{
    if (!t || t->size == 0) {
        return 0;
    }

    uint64_t l = rbtree_hight_rec(t->root->left);
    uint64_t r = rbtree_hight_rec(t->root->right);

    return ((l > r) ? l : r) + 1;
}

static uint64_t rbtree_hight_rec(RbNode *n)
{
    if (!n) {
        return 0;
    }

    uint64_t l = rbtree_hight_rec(n->left);
    uint64_t r = rbtree_hight_rec(n->right);

    return ((l > r) ? l : r) + 1;
}

void *rbtree_remove(RbTree *, void *);

void *rbtree_contains(RbTree *, void *);

Vector *rbtree_breadth_first(RbTree *t)
{
    Vector *v = NULL;
    Queue *q = NULL;

    if (!t || t->size == 0) {
        return NULL;
    }

    v = vector_alloc(t->comp);
    vector_put(v, t->root->data);

    if (t->size == 1) {
        return v;
    }
    q = queue_alloc(t->comp);

    if (t->root->left) {
        queue_enqueue(q, t->root->left);
    }
    if (t->root->right) {
        queue_enqueue(q, t->root->right);
    }

    rbtree_breadth_first_rec(q, v);
    queue_free(q);
    return v;
}

static void rbtree_breadth_first_rec(Queue *q, Vector *v)
{
    RbNode *n = queue_dequeue(q);

    if (!n) {
        return;
    }

    if (n->left) {
        queue_enqueue(q, n->left);
    }
    if (n->right) {
        queue_enqueue(q, n->right);
    }

    vector_put(v, n->data);
    rbtree_breadth_first_rec(q, v);
}

Vector *rbtree_depth_first(RbTree *t)
{
    Vector *v = NULL;

    if (!t || t->size == 0) {
        return NULL;
    }

    v = vector_alloc(t->comp);
    vector_put(v, t->root->data);
    rbtree_depth_first_rec(t->root->left, v);
    rbtree_depth_first_rec(t->root->right, v);

    return v;
}

static void rbtree_depth_first_rec(RbNode *n, Vector *v)
{
    if (!n) {
        return;
    }

    vector_put(v, n->data);
    rbtree_depth_first_rec(n->left, v);
    rbtree_depth_first_rec(n->right, v);
}

Vector *rbtree_in_order(RbTree *);

Vector *rbtree_pre_order(RbTree *);

Vector *rbtree_post_order(RbTree *);

int64_t rbtree_size(RbTree *t)
{
    if (!t) {
        return -1;
    }
    return t->size;
}
