// Copyright (C) 2016, 2017, 2023, 2024 Alexander Necheff
// This program is licensed under the terms of the LGPLv3.
// See the COPYING and COPYING.LESSER files that came packaged with this source code for the full terms.

#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <alibc/strbld.h>

/*
 * Doubles the capacity of the specified StrBld or takes no action on error.
 */
static void strbld_realloc(strbld *);

/*
 * Finds the least power of two >= x
 * Used for heap allocation edge cases in strbld_addstr()
 */
static uint32_t next_pow2(uint32_t);

StrBld *strbld_alloc(void)
{
    StrBld *sb = calloc(1, sizeof(*sb));
    sb->max = STRBLD_START_CAP;
    sb->len = 0;
    sb->str = calloc(sb->max, sizeof(char));

    return sb;
}

void strbld_free(StrBld *sb)
{
    if (sb) {
        free(sb->str);
        free(sb);
    }
}

void strbld_addchar(StrBld *sb, const char c)
{
    if (!sb) {
        return;
    }

    if (sb->len >= (sb->max - 1)) {
        strbld_realloc(sb);
    }

    char *ip = sb->str;
    ip += sb->len++;
    *ip = c;
}

void strbld_addstr(StrBld *sb, const char *str)
{
    if (!sb || !str) {
        return;
    }

    // +1 to make sure we are always able to terminate with a nul byte.
    uint32_t size = sb->len + strlen(str) + 1;
    if (sb->max < size) {
        if (size < sb->max * 2) {
            strbld_realloc(sb);
        } else {
            size = next_pow2(size);
            char *old = sb->str;
            char *new = calloc(size, sizeof(char));

            memcpy(new, old, sb->len);
            sb->str = new;
            sb->max = size;

            free(old);
        }
    }

    strcat(sb->str, str);
    sb->len = strlen(sb->str);
}

size_t strbld_strlen(StrBld *sb)
{
    if (sb) {
        return sb->len;
    }
    return -1;
}

char *strbld_getstr(StrBld *sb)
{
    if (!sb) {
        return NULL;
    }

    char *str = calloc(sb->len + 1, sizeof(*str));
    memcpy(str, sb->str, sb->len);

    return str;
}

void strbld_freestr(char *str)
{
    free(str);
}

void strbld_reset(StrBld *sb)
{
    if (sb) {
        sb->len = 0;
        memset(sb->str, 0, sb->max);
    }
}

static void strbld_realloc(StrBld *sb)
{
    char *str = calloc(sb->max * 2, sizeof(*str));

    if (!str) {
        return;
    }

    memcpy(str, sb->str, sb->max);
    free(sb->str);
    sb->str = str;
    sb->max *= 2;
}

static uint32_t next_pow2(uint32_t x)
{
    x = x - 1;
    x = x | (x >> 1);
    x = x | (x >> 2);
    x = x | (x >> 4);
    x = x | (x >> 8);
    x = x | (x >> 16);

    return x + 1;
}
