// Copyright (C) 2023, 2024 Alexander Necheff
// This program is licensed under the terms of the LGPLv3.
// See the COPYING and COPYING.LESSER files that came packaged with this source code for the full terms.

/**
  Displays the version string of the currently linked alib.
  To access include `extern char alib_version[];` in your code.
  */
char alib_version[] = {
#include "version.inc"
};

