// Copyright (C) 2015, 2016, 2017, 2023, 2024 Alexander Necheff
// This program is licensed under the terms of the LGPLv3.
// See the COPYING and COPYING.LESSER files that came packaged with this source code for the full terms.

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include <alibc/vector.h>

/*
 * Doubles the capacity of the specified Vector or takes no action on error.
 */
static void vector_resize(Vector *);

/*
 * Returns true if 0 <= index < vector_size(v), otherwise false.
 */
static bool vector_checkBounds(Vector *, int64_t);

Vector *vector_alloc(int32_t (*comp)(void *, void *))
{
    Vector *v = calloc(1, sizeof(*v));
    v->max = VECTOR_START_CAP;
    v->len = 0;
    v->data = calloc(v->max, sizeof(void *));
    v->comp = comp;

    return v;
}

void vector_free(Vector *v)
{
    if (!v) {
        return;
    }

    free(v->data);
    free(v);
}

static void vector_resize(Vector *v)
{
    void *new = calloc(v->max * 2, sizeof(void *));
    void *old = v->data;

    if (!new) {
        return;
    }

    memcpy(new, v->data, sizeof(void *) * v->len);
    v->max *= 2;
    v->data = new;
    free(old);
}

void vector_put(Vector *v, void *data)
{
    if (!v) {
        return;
    }

    if (v->max < v->len + 1) {
        vector_resize(v);
    }

    v->data[v->len] = data;
    v->len++;
}

void *vector_get(Vector *v, uint32_t index)
{
    if (!vector_checkBounds(v, index)) {
        return NULL;
    }

    return v->data[index];
}

void *vector_remove(Vector *v, uint32_t index)
{
    void *data = NULL;

    if (!vector_checkBounds(v, index)) {
        return NULL;
    }

    data = v->data[index];

    if (index < v->len - 1) {
        size_t size = (v->len - index - 1) * sizeof(void *);
        memmove((v->data + index), (v->data + index + 1), size);
    }

    v->len--;
    return data;
}

void vector_set(Vector *v, uint32_t index, void *data)
{
    if (!vector_checkBounds(v, index)) {
        return;
    }

    v->data[index] = data;
}

int64_t vector_indexof(Vector *v, void *data)
{
    if (!v) {
        return -1;
    }

    for (uint32_t i = 0; i < v->len; i++) {

        if (! v->comp(data, v->data[i])) {
            return i;
        }
    }

    return -1;
}

void *vector_insert(Vector *v, uint32_t index, void *data)
{
    if (!v || index >= v->len) {
        return NULL;
    }

    if (v->len + 1 > v->max) {
        vector_resize(v);
    }

    size_t size = (v->len - index) * sizeof(void *);
    memmove((v->data + index + 1), (v->data + index), size);
    v->len++;
    vector_set(v, index, data);

    return data;
}

int64_t vector_size(Vector *v)
{
    if (!v) {
        return -1;
    }

    return v->len;
}

static bool vector_checkBounds(Vector *v, int64_t index)
{
    if (v) {
        if (index >= 0 && index < v->len) {
            return true;
        }
    }

    return false;
}
