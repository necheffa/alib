// Copyright (C) 2016, 2017, 2023, 2024 Alexander Necheff
// This program is licensed under the terms of the LGPLv3.
// See the COPYING and COPYING.LESSER files that came packaged with this source code for the full terms.

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include <alibc/hashtbl.h>
#include <alibc/vector.h>

typedef struct Entry Entry;

struct Entry {
    void *value;
    void *key;
};

/*
 * Hashes the provided key to fit in the specified HashTbl.
 *
 * Loudon, Kyle. Mastering Algorithms With C. O'Reilly. 1999. pg 146-147
 */
static uint32_t hashtbl_hash(HashTbl *, void *);

/*
 * Return a pointer to a new Entry or NULL on error.
 */
static Entry *entry_alloc(void *, void *);

/*
 * Return the index of the Entry in the bucket with key
 * or -1 if no such Entry exists for key.
 *
 * returning signed 64 bit integer to accomidate
 * max unsigned 32 bit integer value.
 */
static int64_t bucket_indexof(HashTbl *, Vector *, void *);

/*
 * Frees the specified bucket.
 */
static void bucket_free(Vector *);

static Entry *entry_alloc(void *key, void *value)
{
    Entry *e = calloc(1, sizeof(*e));
    e->key = key;
    e->value = value;

    return e;
}

HashTbl *hashtbl_alloc(int32_t (*comp)(void *, void *))
{
    HashTbl *h = calloc(1, sizeof(*h));
    h->len = 151; // keep prime in order to help "balance" hashtable
    h->comp = comp;
    h->table = calloc(h->len, sizeof(Vector *));

    return h;
}

void hashtbl_free(HashTbl *h)
{
    if (!h || !h->table) {
        return;
    }

    for (uint32_t i = 0; i < h->len; i++) {
        bucket_free(h->table[i]);
    }

    free(h->table);
    free(h);
}

void *hashtbl_put(HashTbl *h, void *key, void *value)
{
    if (!h || !key || !value) {
        return NULL;
    }

    uint32_t hash = hashtbl_hash(h, key);
    Entry *e = entry_alloc(key, value);

    if (!h->table[hash]) {
        h->table[hash] = vector_alloc(h->comp);
        vector_put(h->table[hash], e);
        return NULL;
    }

    Vector *bucket = h->table[hash];
    int64_t index = bucket_indexof(h, bucket, key);

    if (index < 0) {
        vector_put(bucket, e);
        return NULL;
    }

    Entry *old = vector_get(bucket, index);
    vector_set(bucket, index, e);
    return old->value;
}

void *hashtbl_get(HashTbl *h, void *key)
{
    uint32_t hash = 0;
    void *val = NULL;

    if (!h || !key) {
        return NULL;
    }

    hash = hashtbl_hash(h, key);

    if (hash > h->len) {
        return NULL;
    }

    Vector *bucket = h->table[hash];
    if (!bucket) {
        return NULL;
    }

    // since raw values are not stored in the Vector
    // but rather an Entry pointer; it is difficult to
    // use vector_indexof()
    int64_t index = bucket_indexof(h, bucket, key);
    if (index >= 0) {
        Entry *e = vector_get(bucket, index);
        val = e->value;
    }

    return val;
}

void *hashtbl_remove(HashTbl *h, void *key)
{
    void *val = NULL;

    if (!h || !key) {
        return NULL;
    }

    uint32_t hash = hashtbl_hash(h, key);
    if (hash > h->len) {
        return NULL;
    }

    Vector *bucket = h->table[hash];
    if (!bucket) {
        return NULL;
    }

    int64_t index = bucket_indexof(h, bucket, key);
    if (index >= 0) {
        Entry *e = vector_remove(bucket, index);
        val = e->value;
    }


    return val;
}

int64_t hashtbl_size(HashTbl *h)
{
    if (!h || !h->table) {
        return -1;
    }

    uint32_t count = 0;
    int32_t index = h->len - 1;

    while (0 <= index) {
        if (h->table[index]) {
            Vector *v = h->table[index];
            count += v->len;
        }
        index--;
    }

    return count;
}

static uint32_t hashtbl_hash(HashTbl *h, void *key)
{
    const char *p = key;
    uint32_t val = 0;

    while (*p != '\0') {
        uint32_t tmp = 0;

        val = (val << 4) + (*p);
        if ((tmp = (val & 0xf0000000))) {
            val = val ^ (tmp >> 24);
            val = val ^ tmp;
        }

        p++;
    }

    return val % h->len;
}

static int64_t bucket_indexof(HashTbl *h, Vector *v, void *key)
{
    uint32_t len = v->len;
    for (uint32_t i = 0; i < len; i++) {
        Entry *e = vector_get(v, i);
        if (! h->comp(key, e->key)) {
            return i;
        }
    }

    return -1;
}

static void bucket_free(Vector *v)
{

    if (!v) {
        return;
    }

    for (uint32_t i = 0; i < v->len; i++) {
        Entry *e = vector_get(v, i);
        free(e);
    }

    vector_free(v);
}
