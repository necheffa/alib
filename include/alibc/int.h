#ifndef INT_H
#define INT_H

// Copyright (C) 2016, 2024 Alexander Necheff
// This program is licensed under the terms of the LGPLv3.
// See the COPYING and COPYING.LESSER files that came packaged with this source code for the full terms.

/*
 * Provides an integer comparison function suitable to be used as "comp" in the collections.
 */
int32_t int_comp(void *, void *);

/*
 * Returns a pointer to a heap allocated buffer holding the value of the specified integer.
 * Callers are responsible for passing the pointer to free() when no longer needed.
 */
int *int_key(int);
#endif
