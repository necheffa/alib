#ifndef HASHTBL_H
#define HASHTBL_H

// Copyright (C) 2016, 2017, 2023, 2024 Alexander Necheff
// This program is licensed under the terms of the LGPLv3.
// See the COPYING and COPYING.LESSER files that came packaged with this source code for the full terms.

#include <stdint.h>

#include <alibc/vector.h>

typedef struct HashTbl HashTbl;

struct HashTbl {

    uint32_t len;
    int32_t (*comp)(void *, void *);
    void **table;   // array of vectors
};

/*
 * Returns a pointer to a new HashTbl or NULL on error.
 *
 * comp will return < 0 if a < b; 0 if a == b; or
 *   > 0 if a > b
 *
 * To a C programmer, strong typing is simply pounding on the keyboard.
 * HashTbl makes no guarantees about enforcing a consistent key-value type.
 * comp may simply be a raw byte comparison.
 * Storing more than one key-value type mapping is undefined.
 * The honor system is in full effect - with one or two segfaults.
 */
HashTbl *hashtbl_alloc(int32_t (*comp)(void *, void *));

/*
 * Frees HashTbl allocated by hashtbl_alloc().
 */
void hashtbl_free(HashTbl *);

/*
 * Puts a key-value pair to the specified HashTbl.
 *
 * If the given key already exists in the HashTbl,
 * then the new value will overwrite the old value
 * and the old value will be returned; otherwise
 * NULL will be returned.
 * NULL is also returned on error.
 */
void *hashtbl_put(HashTbl *, void *, void *);

/*
 * Returns the value associated with the given key in the specified HashTbl or NULL on error.
 */
void *hashtbl_get(HashTbl *, void *);

/*
 * Removes the given key from the specified HashTbl.
 * Returns the associated value or NULL on error.
 */
void *hashtbl_remove(HashTbl *, void *);

/*
 * Returns the number of key-value pairs in the table or -1 on error.
 */
int64_t hashtbl_size(HashTbl *);

#endif
