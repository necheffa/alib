#ifndef QUEUE_H
#define QUEUE_H

// Copyright (C) 2017, 2023, 2024 Alexander Necheff
// This program is licensed under the terms of the LGPLv3.
// See the COPYING and COPYING.LESSER files that came packaged with this source code for the full terms.

#include <stdint.h>

#define QUEUE_START_CAP 8

typedef struct Queue Queue;

struct Queue {
    void **data;
    uint32_t start;
    uint32_t end;
    uint32_t size;
    uint32_t max;
    int32_t (*comp)(void *, void *);
};

/*
 * Allocate a new Queue.
 * Returns a pointer to the new Queue or NULL on error.
 *
 * comp will return < 0 if a < b; 0 if a == b; or > 0 if a > b
 */
Queue *queue_alloc(int32_t (*comp)(void *, void *));

/*
 * Frees Queue allocated by queue_alloc().
 */
void queue_free(Queue *);

/*
 * Adds data to the back of the specified Queue.
 * Returns a pointer to the data or NULL on error.
 */
void *queue_enqueue(Queue *, void *);

/*
 * Removes and returns the element at the head of the specified Queue or NULL on error.
 */
void *queue_dequeue(Queue *);


/*
 * Returns a pointer to the element at the head of the specified Queue without removing it or NULL on error.
 */
void *queue_peek(Queue *);

/*
 * Return the number of elements in the queue or -1 on error.
 */
int64_t queue_size(Queue *);
#endif
