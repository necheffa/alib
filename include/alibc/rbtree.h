#ifndef RBTREE_H
#define RBTREE_H

// Copyright (C) 2017, 2023, 2024 Alexander Necheff
// This program is licensed under the terms of the LGPLv3.
// See the COPYING and COPYING.LESSER files that came packaged with this source code for the full terms.

#include <alibc/vector.h>

typedef struct RbTree RbTree;
typedef struct RbNode RbNode;

struct RbTree {
    uint32_t size;
    RbNode *root;
    int32_t (*comp)(void *,void *);
};

/*
 * Return a pointer to a new RbTree or NULL on error.
 *
 * comp will be a function suitable for comparing
 *   data within the tree.
 *   comp will return < 0 if a < b; 0 if a == b; or
 *   > 0 if a > b
 */
RbTree *rbtree_alloc(int32_t (*comp)(void *,void *));

/*
 * Frees RbTree returned by rbtree_alloc().
 * Takes no action if RbTree is NULL.
 */
void rbtree_free(RbTree *);

/*
 * Insert a value into the specified RbTree.
 * Return a pointer to the value if it exists in the tree, otherwise NULL.
 * Returns NULL on error.
 */
void *rbtree_insert(RbTree *, void *);

/*
 * Returns the value from the specified RbTree after removing it.
 * Returns NULL if the value did not exist in the RbTree or on error.
 */
void *rbtree_remove(RbTree *, void *);

/*
 * Return a pointer to the value in the specified RbTree.
 * Returns NULL if the value did not exist in the RbTree or on error.
 */
void *rbtree_contains(RbTree *, void *);

/*
 * Return the hight of the specified RbTree.
 */
uint64_t rbtree_hight(RbTree *);

/*
 * Returns a Vector of pointers to all values in the specified RbTree in breadth first order or NULL on error.
 */
Vector *rbtree_breadth_first(RbTree *);

/*
 * Returns a Vector of pointers to all values in the specified RbTree in depths first order or NULL on error.
 */
Vector *rbtree_depth_first(RbTree *);

/*
 * Returns a Vector of pointers to all values in the specified RbTree in first order or NULL on error.
 */
Vector *rbtree_in_order(RbTree *);

/*
 * Returns a Vector of pointers to all values in the specified RbTree in prefix first order or NULL on error.
 */
Vector *rbtree_pre_order(RbTree *);

/*
 * Returns a Vector of pointers to all values in the specified RbTree in postfix first order or NULL on error.
 */
Vector *rbtree_post_order(RbTree *);

/*
 * Return the number of elements in the RbTree or -1 on error.
 */
int64_t rbtree_size(RbTree *);
#endif
