#ifndef VECTOR_H
#define VECTOR_H

// Copyright (C) 2015, 2016, 2017, 2023, 2024 Alexander Necheff
// This program is licensed under the terms of the LGPLv3.
// See the COPYING and COPYING.LESSER files that came packaged with this source code for the full terms.

#include <stdint.h>

/*
 * Provides the starting maximum capacity of a Vector.
 */
#define VECTOR_START_CAP 8

typedef struct Vector Vector;

struct Vector {
    uint32_t max;
    uint32_t len;
    void **data;
    int32_t (*comp)(void *, void *);
};

/*
 * Allocate a new Vector.
 * Returns a pointer to the new Vector or NULL on error.
 *
 * comp will return < 0 if a < b; 0 if a == b; or
 *   > 0 if a > b
 */
Vector *vector_alloc(int32_t (*comp)(void *, void *));

/*
 * Frees Vector allocated by vector_alloc().
 * Memory allocated to elements is not freed however.
 */
void vector_free(Vector *);

/*
 * Puts data into the specified Vector.
 */
void vector_put(Vector *, void *);

/*
 * Removes data from Vector at the given index.
 * Returns a pointer to the data or NULL on error.
 */
void *vector_remove(Vector *, uint32_t);

/*
 * Gets data from Vector at the given index without removing it from the Vector.
 * Returns a pointer to the data or NULL on error.
 */
void *vector_get(Vector *, uint32_t);

/*
 * Sets the data at a given index in the Vector.
 * 0 <= index <= vector_size(v) must be true otherwise no action is taken.
 */
void vector_set(Vector *, uint32_t, void *);

/*
 * Returns index of first occurrence of data in Vector or -1 if not found.
 */
int64_t vector_indexof(Vector *, void *);

/*
 * Insert the data at the given index, shifting the old data to the right.
 * Returns a copy of the data on success, or NULL on failure.
 * 0 <= index < vector_size(v) must be true.
 */
void *vector_insert(Vector *, uint32_t, void *);

/*
 * Returns the number of elements within the specified Vector or -1 on error.
 */
int64_t vector_size(Vector *);

#endif
