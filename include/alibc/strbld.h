#ifndef STRBLD_H
#define STRBLD_H

// Copyright (C) 2016, 2023, 2024 Alexander Necheff
// This program is licensed under the terms of the LGPLv3.
// See the COPYING and COPYING.LESSER files that came packaged with this source code for the full terms.

#define STRBLD_START_CAP 8

typedef struct StrBld strbld;
typedef struct StrBld StrBld;

struct StrBld {
    uint32_t len;
    uint32_t max;
    char *str;
};

/*
 * Return a pointer to a new StrBld or NULL on error.
 */
strbld *strbld_alloc(void);

/*
 * Frees StrBld returned by strbld_alloc().
 * The underlying string is also freed.
 */
void strbld_free(strbld *);

/*
 * Appends a single char to the specified StrBld.
 */
void strbld_addchar(strbld *, const char);

/*
 * Appends a string to the specified StrBld.
 */
void strbld_addstr(strbld *, const char *);

/*
 * Returns the length of the specified StrBld or -1 on error.
 */
size_t strbld_strlen(strbld *);

/*
 * Returns a copy of the string managed by the specified StrBld.
 * Callers must use strbld_freestr() to release the copied string.
 */
char *strbld_getstr(strbld *);

/*
 * Frees string returned by strbld_getstr().
 */
void strbld_freestr(char *);

/*
 * Empties the specified StrBld, setting its length to 0, but without altering its current capacity.
 */
void strbld_reset(strbld *);

#endif
