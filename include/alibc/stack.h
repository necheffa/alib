#ifndef STACK_H
#define STACK_H

// Copyright (C) 2016, 2023, 2024 Alexander Necheff
// This program is licensed under the terms of the LGPLv3.
// See the COPYING and COPYING.LESSER files that came packaged with this source code for the full terms.

#include <stdint.h>

#include <alibc/vector.h>

/* they'll never know */
typedef struct Vector Stack;

/*
 * Allocate a new Stack.
 * Returns a pointer to the allocated Stack or NULL on error.
 *
 * comp will be a function suitable for comparing
 *   data within the stack; < 0 if a < b; 0 if a == b
 *   or > 0 if a > b
 */
Stack *stack_alloc(int32_t (*comp)(void *, void *));

/*
 * Frees a Stack allocated by stack_alloc().
 * Element memory is not freed.
 */
void stack_free(Stack *);

/*
 * Pushes data on to the specified Stack.
 */
void stack_push(Stack *, void *);

/*
 * Returns the element on the top of the Stack without removing the element from the Stack.
 * Returns NULL on error or if the Stack is empty.
 */
void *stack_peek(Stack *);

/*
 * Returns the element on top of the Stack, removing that element from the Stack.
 * Returns NULL on error or if the Stack is empty.
 */
void *stack_pop(Stack *);

/*
 * Return the number of elements in the specified Stack or -1 on error.
 */
int64_t stack_size(Stack *);
#endif
