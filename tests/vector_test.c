#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <setjmp.h>
#include <cmocka.h>

#include <alibc/vector.h>
#include <alibc/int.h>

#define MAX 8192

// A number to exceed internal vector reallocation.
static const int REALLOC = VECTOR_START_CAP + 4;

// When vector_put() is called on a valid, empty vector, values are placed in the vector.
static void test_vector_put(void **state)
{
    Vector *v = vector_alloc(int_comp);
    for (int i = 0; i < REALLOC; i++) {
        vector_put(v, int_key(i));
    }

    for (int i = 0; i < REALLOC; i++) {
        int *k = vector_get(v, i);
        assert_true(*k == i);
    }
    vector_free(v);
}

// When vector_put() is called on a NULL vector, no action is taken.
static void test_vector_put_null(void **state)
{
    Vector *v = NULL;
    vector_put(v, int_key(999));
    assert_true(vector_size(v) == -1);
    vector_free(v); // mini-test for no action taken on freeing NULL.
}

// When vector_size() is called on a valid, populated vector, the size is returned.
static void test_vector_size(void **state)
{
    Vector *v = vector_alloc(int_comp);

    for (int i = 0; i < MAX; i++) {
        vector_put(v, int_key(i));
    }

    assert_true(vector_size(v) == MAX);
    vector_free(v);
}

// When vector_size() is called on a NULL vector, -1 is returned.
static void test_vector_size_null(void **state)
{
    assert_true(vector_size(NULL) == -1);
}

// When vector_size() is called on a valid, empty vector, 0 is returned.
static void test_vector_size_empty(void **state)
{
    Vector *v = vector_alloc(int_comp);
    assert_true(vector_size(v) == 0);
}

// When vector_get() is passed a valid, populated vector, values are returned in the expected sequence.
static void test_vector_get(void **state)
{
    Vector *v = vector_alloc(int_comp);

    for (int i = 0; i < REALLOC; i++) {
        vector_put(v, int_key(i));
    }

    for (int i = 0; i < REALLOC; i++) {
        int *k = vector_get(v, i);
        assert_true(*k == i);
    }

    vector_free(v);
}

// When vector_get() is passed a NULL pointer, NULL is returned.
static void test_vector_get_null(void **state)
{
    Vector *v = NULL;
    assert_true(vector_get(v, 0) == NULL);
}

// When vector_get() is passed an empty but non-NULL vector, NULL is returned.
static void test_vector_get_empty(void **state)
{
    Vector *v = vector_alloc(int_comp);
    assert_true(vector_get(v, 0) == NULL);
    vector_free(v);
}

// When vector_get() requests an index past the end of a valid, populated vector, NULL is returned.
static void test_vector_get_pasttheend(void **state)
{
    Vector *v = vector_alloc(int_comp);
    for (int i = 0; i < REALLOC; i++) {
        vector_put(v, int_key(i));
    }

    assert_true(vector_get(v, REALLOC) == NULL);
    vector_free(v);
}

// When vector_get() requests an index below the start of a valid, populated vector, NULL is returned.
static void test_vector_get_belowthestart(void **state)
{
    Vector *v = vector_alloc(int_comp);
    for (int i = 0; i < REALLOC; i++) {
        vector_put(v, int_key(i));
    }

    assert_true(vector_get(v, -1) == NULL);
    vector_free(v);
}

// When vector_set() sets an index within the bounds of a valid vector, the value is set.
static void test_vector_set(void **state)
{
    Vector *v = vector_alloc(int_comp);
    for (int i = 0; i < REALLOC; i++) {
        vector_put(v, int_key(i));
    }

    vector_set(v, 2, int_key(999));

    assert_true(vector_size(v) == REALLOC);
    for (int i = 0; i < REALLOC; i++) {
        int *k = vector_get(v, i);
        if (i == 2) {
            assert_true(*k == 999);
        } else {
            assert_true(*k == i);
        }
    }

    vector_free(v);
}

// When vector_set() sets an index in a valid, empty vector, no action is taken.
static void test_vector_set_empty(void **state)
{
    Vector *v = vector_alloc(int_comp);
    vector_set(v, 0, int_key(999));
    assert_true(vector_size(v) == 0);
    vector_free(v);
}

// When vector_set() sets an index in a NULL vector, no action is taken.
static void test_vector_set_null(void **state)
{
    Vector *v = NULL;
    vector_set(v, 0, int_key(999));
    assert_true(vector_size(v) == -1);
}

// When vector_set() sets an index past the end of a valid, populated vector, no action is taken.
static void test_vector_set_pasttheend(void **state)
{
    Vector *v = vector_alloc(int_comp);
    for (int i = 0; i < REALLOC; i++) {
        vector_put(v, int_key(i));
    }

    int k = 2;
    vector_set(v, REALLOC, &k);
    assert_true(vector_size(v) == REALLOC);
    vector_free(v);
}

// When vector_set() sets an index below the start of a valid, populated vector, no action is taken.
static void test_vector_set_belowthestart(void **state)
{
    Vector *v = vector_alloc(int_comp);
    for (int i = 0; i < REALLOC; i++) {
        vector_put(v, int_key(i));
    }

    int k = 2;
    vector_set(v, -1, &k);
    assert_true(vector_size(v) == REALLOC);
    vector_free(v);
}

// When vector_remove() is passed an empty vector, NULL is returned.
static void test_vector_remove_empty(void **state)
{
    Vector *v = vector_alloc(int_comp);
    assert_true(vector_remove(v, 0) == NULL);
    assert_true(vector_size(v) == 0);
    vector_free(v);
}

// When vector_remove() is passed a NULL vector, NULL is returned.
static void test_vector_remove_null(void **state)
{
    Vector *v = NULL;
    assert_true(vector_remove(v, 0) == NULL);
    assert_true(vector_size(v) == -1);
}

// When vector_remove() is passed an index past the end of a valid vector, NULL is returned.
static void test_vector_remove_pasttheend(void **state)
{
    Vector *v = vector_alloc(int_comp);
    vector_put(v, int_key(999));
    assert_true(vector_remove(v,1) == NULL);
    assert_true(vector_size(v) == 1);
    vector_free(v);
}

// When vector_remove() is passed an index below the start of a valid vector, NULL is returned.
static void test_vector_remove_belowthestart(void **state)
{
    Vector *v = vector_alloc(int_comp);
    vector_put(v, int_key(999));
    assert_true(vector_remove(v,-1) == NULL);
    assert_true(vector_size(v) == 1);
    vector_free(v);
}

// When vector_remove() is passed the last index of a valid 3 element vector, it is removed.
static void test_vector_remove_last(void **state)
{
    Vector *v = vector_alloc(int_comp);
    vector_put(v, int_key(0));
    vector_put(v, int_key(1));
    vector_put(v, int_key(2));

    int *val = vector_remove(v, 2);
    assert_true(*val == 2);
    assert_true(vector_size(v) == 2);

    val = vector_get(v, 0);
    assert_true(*val == 0);
    val = vector_get(v, 1);
    assert_true(*val == 1);

    vector_free(v);
}

// When vector_remove() is passed the middle index of a valid 3 element vector, it is removed.
static void test_vector_remove_middle(void **state)
{
    Vector *v = vector_alloc(int_comp);
    vector_put(v, int_key(0));
    vector_put(v, int_key(1));
    vector_put(v, int_key(2));

    int *val = vector_remove(v, 1);
    assert_true(*val == 1);
    assert_true(vector_size(v) == 2);

    val = vector_get(v, 0);
    assert_true(*val == 0);
    val = vector_get(v, 1);
    assert_true(*val == 2);

    vector_free(v);
}

// When vector_remove() is passed the first index of a valid 3 element vector, it is removed.
static void test_vector_remove_first(void **state)
{
    Vector *v = vector_alloc(int_comp);
    vector_put(v, int_key(0));
    vector_put(v, int_key(1));
    vector_put(v, int_key(2));

    int *val = vector_remove(v, 0);
    assert_true(*val == 0);
    assert_true(vector_size(v) == 2);

    val = vector_get(v, 0);
    assert_true(*val == 1);
    val = vector_get(v, 1);
    assert_true(*val == 2);

    vector_free(v);
}

// When vector_remove() is passed the first index of a valid 1 element vector, it is removed.
static void test_vector_remove_single(void **state)
{
    Vector *v = vector_alloc(int_comp);
    vector_put(v, int_key(0));

    int *val = vector_remove(v, 0);
    assert_true(*val == 0);
    assert_true(vector_size(v) == 0);

    val = vector_get(v, 0);
    assert_true(val == NULL);

    vector_free(v);
}

// When vector_insert() is passed the first index of a valid 3 element vector, it adds the value as the first element.
static void test_vector_insert_first(void **state)
{
    Vector *v = vector_alloc(int_comp);
    vector_put(v, int_key(0));
    vector_put(v, int_key(1));
    vector_put(v, int_key(2));

    int *val = vector_insert(v, 0, int_key(999));
    assert_true(val != NULL);
    assert_true(*val == 999);
    assert_true(vector_size(v) == 4);

    val = NULL;
    val = vector_get(v, 0);
    assert_true(*val == 999);
    val = NULL;
    val = vector_get(v, 1);
    assert_true(*val == 0);
    val = NULL;
    val = vector_get(v, 2);
    assert_true(*val == 1);
    val = NULL;
    val = vector_get(v, 3);
    assert_true(*val == 2);

    vector_free(v);
}

// When vector_insert() is passed the second index of a valid 3 element vector, it adds the value as the second element.
static void test_vector_insert_second(void **state)
{
    Vector *v = vector_alloc(int_comp);
    vector_put(v, int_key(0));
    vector_put(v, int_key(1));
    vector_put(v, int_key(2));

    int *val = vector_insert(v, 1, int_key(999));
    assert_true(val != NULL);
    assert_true(*val == 999);
    assert_true(vector_size(v) == 4);

    val = NULL;
    val = vector_get(v, 0);
    assert_true(*val == 0);
    val = NULL;
    val = vector_get(v, 1);
    assert_true(*val == 999);
    val = NULL;
    val = vector_get(v, 2);
    assert_true(*val == 1);
    val = NULL;
    val = vector_get(v, 3);
    assert_true(*val == 2);

    vector_free(v);
}

// When vector_insert() is passed the third index of a valid 3 element vector, it adds the value as the third element.
static void test_vector_insert_third(void **state)
{
    Vector *v = vector_alloc(int_comp);
    vector_put(v, int_key(0));
    vector_put(v, int_key(1));
    vector_put(v, int_key(2));

    int *val = vector_insert(v, 2, int_key(999));
    assert_true(val != NULL);
    assert_true(*val == 999);
    assert_true(vector_size(v) == 4);

    val = NULL;
    val = vector_get(v, 0);
    assert_true(*val == 0);
    val = NULL;
    val = vector_get(v, 1);
    assert_true(*val == 1);
    val = NULL;
    val = vector_get(v, 2);
    assert_true(*val == 999);
    val = NULL;
    val = vector_get(v, 3);
    assert_true(*val == 2);

    vector_free(v);
}

// When vector_insert() is passed an index passed the end of a valid vector, it returns NULL.
static void test_vector_insert_pasttheend(void **state)
{
    Vector *v = vector_alloc(int_comp);
    vector_put(v, int_key(0));

    int *val = vector_insert(v, 1, int_key(0));
    assert_true(val == NULL);
    assert_true(vector_size(v) == 1);
    val = vector_get(v, 0);
    assert_true(val != NULL);
    assert_true(*val == 0);

    vector_free(v);
}

// When vector_insert() is passed an index below the start of a valid vector, it returns NULL.
static void test_vector_insert_belowthestart(void **state)
{
    Vector *v = vector_alloc(int_comp);
    vector_put(v, int_key(0));

    int *val = vector_insert(v, -1, int_key(0));
    assert_true(val == NULL);
    assert_true(vector_size(v) == 1);
    val = vector_get(v, 0);
    assert_true(val != NULL);
    assert_true(*val == 0);

    vector_free(v);
}

// When vector_insert() is passed an empty vector, it returns NULL.
static void test_vector_insert_empty(void **state)
{
    Vector *v = vector_alloc(int_comp);

    int *val = vector_insert(v, 0, int_key(0));
    assert_true(val == NULL);
    assert_true(vector_size(v) == 0);
}

// When vector_insert() is passed a NULL vector, it returns NULL.
static void test_vector_insert_null(void **state)
{
    Vector *v = NULL;

    int *val = vector_insert(v, 0, int_key(0));
    assert_true(val == NULL);
    assert_true(vector_size(v) == -1);
}

// When vector_insert() causes an internal reallocation, the data remains in tact.
static void test_vector_insert_realloc(void **state)
{
    Vector *v = vector_alloc(int_comp);

    for (int i = 0; i < VECTOR_START_CAP; i++) {
        vector_put(v, int_key(i));
    }

    int *val = vector_insert(v, 5, int_key(999));
    assert_true(*val == 999);
    assert_true(vector_size(v) == VECTOR_START_CAP + 1);

    for (int i = 0; i < VECTOR_START_CAP + 1; i++) {
        int *val1 = vector_get(v, i);
        if (i == 5) {
            assert_true(*val1 == 999);
        } else if (i < 5) {
            assert_true(*val1 == i);
        } else {
            assert_true(*val1 == i - 1);
        }
    }

    for (int i = 0; i < VECTOR_START_CAP + 1; i++) {
        int *val1 = vector_get(v, i);
        free(val1);
    }
    vector_free(v);
}

// When vector_indexof() is called on a single element vector with a match, it is returned.
static void test_vector_indexof_single(void **state)
{
    Vector *v = vector_alloc(int_comp);
    vector_put(v, int_key(0));

    assert_true(vector_indexof(v, int_key(0)) == 0);

    vector_free(v);
}

// When vector_indexof() is called on a three element vector with a match, it is returned.
static void test_vector_indexof_tripple(void **state)
{
    Vector *v = vector_alloc(int_comp);
    vector_put(v, int_key(0));
    vector_put(v, int_key(1));
    vector_put(v, int_key(2));

    assert_true(vector_indexof(v, int_key(2)) == 2);

    vector_free(v);
}

// When vector_indexof() is called on a three element vector with three matches, only the first index is returned.
static void test_vector_indexof_tripplematch(void **state)
{
    Vector *v = vector_alloc(int_comp);
    vector_put(v, int_key(0));
    vector_put(v, int_key(0));
    vector_put(v, int_key(0));

    assert_true(vector_indexof(v, int_key(0)) == 0);

    vector_free(v);
}

// When vector_indexof() is called on a valid, populated vector, with no matches, -1 is returned.
static void test_vector_indexof_nomatch(void **state)
{
    Vector *v = vector_alloc(int_comp);
    vector_put(v, int_key(0));
    vector_put(v, int_key(0));
    vector_put(v, int_key(0));

    assert_true(vector_indexof(v, int_key(1)) == -1);

    vector_free(v);
}

// When vector_indexof() is called on an empty vector, -1 is returned.
static void test_vector_indexof_empty(void **state)
{
    Vector *v = vector_alloc(int_comp);

    assert_true(vector_indexof(v, int_key(0)) == -1);

    vector_free(v);
}

// When vector_indexof() is called on a NULL vector, -1 is returned.
static void test_vector_indexof_null(void **state)
{
    Vector *v = NULL;

    assert_true(vector_indexof(v, int_key(0)) == -1);

    vector_free(v);
}

int vector_tests(void)
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_vector_put),
        cmocka_unit_test(test_vector_put_null),
        cmocka_unit_test(test_vector_size),
        cmocka_unit_test(test_vector_size_null),
        cmocka_unit_test(test_vector_size_empty),
        cmocka_unit_test(test_vector_get),
        cmocka_unit_test(test_vector_get_null),
        cmocka_unit_test(test_vector_get_empty),
        cmocka_unit_test(test_vector_get_pasttheend),
        cmocka_unit_test(test_vector_get_belowthestart),
        cmocka_unit_test(test_vector_set),
        cmocka_unit_test(test_vector_set_empty),
        cmocka_unit_test(test_vector_set_null),
        cmocka_unit_test(test_vector_set_pasttheend),
        cmocka_unit_test(test_vector_set_belowthestart),
        cmocka_unit_test(test_vector_remove_empty),
        cmocka_unit_test(test_vector_remove_null),
        cmocka_unit_test(test_vector_remove_pasttheend),
        cmocka_unit_test(test_vector_remove_belowthestart),
        cmocka_unit_test(test_vector_remove_last),
        cmocka_unit_test(test_vector_remove_middle),
        cmocka_unit_test(test_vector_remove_first),
        cmocka_unit_test(test_vector_remove_single),
        cmocka_unit_test(test_vector_insert_first),
        cmocka_unit_test(test_vector_insert_second),
        cmocka_unit_test(test_vector_insert_third),
        cmocka_unit_test(test_vector_insert_pasttheend),
        cmocka_unit_test(test_vector_insert_belowthestart),
        cmocka_unit_test(test_vector_insert_empty),
        cmocka_unit_test(test_vector_insert_null),
        cmocka_unit_test(test_vector_insert_realloc),
        cmocka_unit_test(test_vector_indexof_single),
        cmocka_unit_test(test_vector_indexof_tripple),
        cmocka_unit_test(test_vector_indexof_tripplematch),
        cmocka_unit_test(test_vector_indexof_nomatch),
        cmocka_unit_test(test_vector_indexof_empty),
        cmocka_unit_test(test_vector_indexof_null),
    };

    return cmocka_run_group_tests_name("vector", tests, NULL, NULL);
}

