#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <setjmp.h>
#include <cmocka.h>

#include <stdlib.h>
#include <string.h>

#include <alibc/strbld.h>

// When strbld_strlen() is passed an empty StrBld, 0 is returned.
static void test_strbld_strlen_empty(void **state)
{
    StrBld *sb = strbld_alloc();
    assert_true(strbld_strlen(sb) == 0);
    strbld_free(sb);
}

// When strbld_strlen() is passed a NULL StrBld, -1 is returned.
static void test_strbld_strlen_null(void **state)
{
    StrBld *sb = NULL;
    assert_true(strbld_strlen(sb) == -1);
    strbld_free(sb);
}

// When strbld_addchar() is passed a valid StrBld and a valid char, the char is added.
static void test_strbld_addchar(void **state)
{
    StrBld *sb = strbld_alloc();
    char *str = "This is a large string.";
    char *p = str;
    size_t len = strlen(str);

    for (uint32_t i = 1; i <= len; i++) {
        strbld_addchar(sb, *p);
        assert_true(strbld_strlen(sb) == i);
        p++;
    }

    char *s = strbld_getstr(sb);
    assert_true(strcmp(s, str) == 0);

    strbld_free(sb);
    free(s);
}

// When strbld_addchar() is passed a NULL StrBld, no action is taken.
static void test_strbld_addchar_null(void **state)
{
    StrBld *sb = NULL;
    strbld_addchar(sb, 'c');
}

// When strbld_addstr() is passed a valid empty StrBld and a valid string, the string is added.
static void test_strbld_addstr(void **state)
{
    StrBld *sb = strbld_alloc();
    char *str = "This is a large string.";
    size_t len = strlen(str);

    strbld_addstr(sb, str);

    assert_true(strbld_strlen(sb) == len);

    char *s = strbld_getstr(sb);
    assert_true(strcmp(s, str) == 0);

    strbld_free(sb);
    free(s);
}

// When strbld_addstr() is passed a valid populated StrBld and a valid string, the string is appended.
static void test_strbld_addstr_append(void **state)
{
    StrBld *sb = strbld_alloc();
    char *str = "This is a large string.";
    char *strdbl = "This is a large string.This is a large string.";
    size_t len = strlen(strdbl);

    strbld_addstr(sb, str);
    strbld_addstr(sb, str);

    assert_true(strbld_strlen(sb) == len);

    char *s = strbld_getstr(sb);
    assert_true(strcmp(s, strdbl) == 0);

    strbld_free(sb);
    free(s);
}

// When strbld_addstr() is passed a NULL StrBld, no action is taken.
static void test_strbld_addstr_null(void **state)
{
    StrBld *sb = NULL;
    char *str = "This is a string.";

    strbld_addstr(sb, str);
}

// When strbld_addstr() is passed a valid StrBld and a NULL string, no action is taken.
static void test_strbld_addstr_nullstring(void **state)
{
    StrBld *sb = strbld_alloc();
    char *str = NULL;

    strbld_addstr(sb, str);
}

// When strbld_addstr() is passed a valid StrBld and an empty string, no action is taken.
static void test_strbld_addstr_emptystring(void **state)
{
    StrBld *sb = strbld_alloc();
    char *str = "";

    strbld_addstr(sb, str);
    assert_true(strbld_strlen(sb) == 0);
}

// When strbld_getstr() is passed a valid, populated StrBld, the string is returned.
static void test_strbld_getstr(void **state)
{
    StrBld *sb = strbld_alloc();
    char *str = "12345678";
    strbld_addstr(sb, str);

    char *s = strbld_getstr(sb);
    assert_true(strbld_strlen(sb) == strlen(str));
    assert_true(strcmp(s, str) == 0);

    strbld_free(sb);
    free(s);
}

// When strbld_getstr() is passed a valid, empty StrBld, the empty string is returned.
static void test_strbld_getstr_empty(void **state)
{
    StrBld *sb = strbld_alloc();
    char *s = strbld_getstr(sb);

    assert_true(strbld_strlen(sb) == 0);
    assert_true(strcmp(s, "") == 0);

    strbld_free(sb);
    free(s);
}

// When strbld_getstr() is passed a NULL StrBld, NULL is returned.
static void test_strbld_getstr_null(void **state)
{
    StrBld *sb = NULL;
    char *s = strbld_getstr(sb);

    assert_true(s == NULL);
}

// When strbld_reset() is passed a valid, populated StrBld, it is made empty again.
static void test_strbld_reset(void **state)
{
    StrBld *sb = strbld_alloc();
    char *str = "123456789";
    strbld_addstr(sb, str);

    strbld_reset(sb);

    assert_true(strbld_strlen(sb) == 0);
    char *s = strbld_getstr(sb);
    assert_true(strcmp(s, "") == 0);

    strbld_free(sb);
    free(s);
}

// When strbld_reset() is passed a valid, empty StrBld, no action is taken.
static void test_strbld_reset_empty(void **state)
{
    StrBld *sb = strbld_alloc();
    strbld_reset(sb);

    assert_true(strbld_strlen(sb) == 0);
    char *s = strbld_getstr(sb);
    assert_true(strcmp(s, "") == 0);

    strbld_free(sb);
    free(s);
}

// When strbld_reset() is passed a NULL StrBld, no action is taken.
static void test_strbld_reset_null(void **state)
{
    StrBld *sb = NULL;
    strbld_reset(sb);

    assert_true(strbld_strlen(sb) == -1);
    char *s = strbld_getstr(sb);
    assert_true(s == NULL);

    strbld_free(sb);
    free(s);
}

int strbld_tests(void)
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_strbld_strlen_empty),
        cmocka_unit_test(test_strbld_strlen_null),
        cmocka_unit_test(test_strbld_addchar),
        cmocka_unit_test(test_strbld_addchar_null),
        cmocka_unit_test(test_strbld_addstr),
        cmocka_unit_test(test_strbld_addstr_append),
        cmocka_unit_test(test_strbld_addstr_null),
        cmocka_unit_test(test_strbld_addstr_nullstring),
        cmocka_unit_test(test_strbld_addstr_emptystring),
        cmocka_unit_test(test_strbld_getstr),
        cmocka_unit_test(test_strbld_getstr_empty),
        cmocka_unit_test(test_strbld_getstr_null),
        cmocka_unit_test(test_strbld_reset),
        cmocka_unit_test(test_strbld_reset_empty),
        cmocka_unit_test(test_strbld_reset_null),
    };

    return cmocka_run_group_tests_name("strbld", tests, NULL, NULL);
}

