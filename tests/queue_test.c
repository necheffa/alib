#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <setjmp.h>
#include <stdbool.h>
#include <cmocka.h>

#include <alibc/queue.h>
#include <alibc/int.h>

#define MAX 8192

// A number to exceed internal vector reallocation.
static const int REALLOC = QUEUE_START_CAP + 4;

static bool queue_iswrapped(Queue *q)
{
    if (q) {
        return q->start > q->end;
    }

    return false;
}

// When queue_size() is passed a valid, populated queue, the correct size is returned.
static void test_queue_size(void **state)
{
    Queue *q = queue_alloc(int_comp);
    for (int i = 1; i <= REALLOC; i++) {
        queue_enqueue(q, int_key(i));
        assert_true(queue_size(q) == i);
    }
    queue_free(q);
}

// When queue_size() is passed a valid, empty queue, 0 is returned.
static void test_queue_size_empty(void **state)
{
    Queue *q = queue_alloc(int_comp);
    assert_true(queue_size(q) == 0);
    queue_free(q);
}

// When queue_size() is passed a valid, empty queue, after dequeuing, 0 is returned.
static void test_queue_size_empty_dequeue(void **state)
{
    Queue *q = queue_alloc(int_comp);
    for (int i = 1; i <= REALLOC; i++) {
        queue_enqueue(q, int_key(i));
    }
    for (int i = 1; i <= REALLOC; i++) {
        queue_dequeue(q);
    }

    assert_true(queue_size(q) == 0);
    queue_free(q);
}

// When queue_size() is passed a valid, empty wrapped queue, after dequeuing, 0 is returned.
static void test_queue_size_empty_dequeue_wrap(void **state)
{
    Queue *q = queue_alloc(int_comp);
    // recall, we reallocate at q->max - 1, not q->max.
    for (int i = 0; i < QUEUE_START_CAP - 1; i++) {
        queue_enqueue(q, int_key(999));
    }

    assert_true(queue_size(q) == 7);
    queue_dequeue(q);
    assert_true(queue_size(q) == 6);
    queue_enqueue(q, int_key(999));
    assert_true(queue_size(q) == 7);

    assert_true(queue_iswrapped(q));

    queue_enqueue(q, int_key(999));
    assert_true(queue_size(q) == 8);

    for (int i = 0; i < QUEUE_START_CAP; i++) {
        queue_dequeue(q);
    }

    assert_true(queue_size(q) == 0);
    queue_free(q);
}

// When queue_size() is passed a NULL queue, -1 is returned.
static void test_queue_size_null(void **state)
{
    Queue *q = NULL;
    assert_true(queue_size(q) == -1);
    queue_free(q);
}

// When queue_enqueue() is passed a valid queue, the elements are enqueued in order.
static void test_queue_enqueue(void **state)
{
    Queue *q = queue_alloc(int_comp);
    for (int i = 0; i < REALLOC; i++) {
        int *val = queue_enqueue(q, int_key(i));
        assert_true(*val == i);
    }
    assert_true(queue_size(q) == REALLOC);

    for (int i = 0; i < REALLOC; i++) {
        int *val = queue_dequeue(q);
        assert_true(*val == i);
    }
    assert_true(queue_size(q) == 0);

    queue_free(q);
}

// When queue_enqueue() is passed a valid queue that is caused to wrap, queue order is maintained.
static void test_queue_enqueue_wrap(void **state)
{
    Queue *q = queue_alloc(int_comp);
    int *val;

    // recall, we reallocate at q->max - 1, not q->max.
    for (int i = 0; i < QUEUE_START_CAP - 1; i++) {
        val = queue_enqueue(q, int_key(i));
        assert_true(*val == i);
    }
    val = queue_dequeue(q);
    assert_true(*val == 0);
    assert_true(queue_size(q) == 6);
    queue_enqueue(q, int_key(7));
    assert_true(queue_size(q) == 7);
    assert_true(queue_iswrapped(q));

    for (int i = 1; i < QUEUE_START_CAP; i++) {
        val = queue_dequeue(q);
        assert_true(*val == i);
    }

    assert_true(queue_size(q) == 0);
    queue_free(q);
}

// When queue_enqueue() is passed a valid queue that is caused to wrap and reallocate, queue order is maintained.
static void test_queue_enqueue_wrap_realloc(void **state)
{
    Queue *q = queue_alloc(int_comp);

    // make sure the underlying vector is fully allocated,
    // recall, we reallocate at q->max - 1, not q->max.
    for (int i = 0; i < QUEUE_START_CAP - 1; i++) {
        queue_enqueue(q, int_key(90 + i));
    }

    // jiggle the handle: keep the queue full, but make sure q->start != 0
    queue_dequeue(q); // gets rid of 90
    queue_enqueue(q, int_key(999));

    assert_true(queue_iswrapped(q));
    // now, force a reorder and a reallocation
    queue_enqueue(q, int_key(1000));
    queue_enqueue(q, int_key(1001));

    assert_true(queue_size(q) == QUEUE_START_CAP + 1);

    for (int i = 1; i < QUEUE_START_CAP - 1; i++) {
        int *val = queue_dequeue(q);
        assert_true(*val == 90 + i);
    }
    int *val = queue_dequeue(q);
    assert_true(*val == 999);
    val = queue_dequeue(q);
    assert_true(*val == 1000);
    val = queue_dequeue(q);
    assert_true(*val == 1001);

    assert_true(queue_size(q) == 0);
    queue_free(q);
}

// When queue_enqueue() is passed a valid queue that is wrapped and at the end of the underlying array, queue order is maintained.
static void test_queue_enqueue_wrap_end(void **state)
{
    Queue *q = queue_alloc(int_comp);

    for (int i = 0; i < QUEUE_START_CAP - 2; i++) {
        queue_enqueue(q, int_key(i));
    }
    int *val = queue_dequeue(q);
    val = queue_dequeue(q);

    assert_true(queue_size(q) == 4);

    queue_enqueue(q, int_key(997));
    assert_true(queue_size(q) == 5);

    queue_enqueue(q, int_key(998));
    assert_true(queue_size(q) == 6);

    queue_enqueue(q, int_key(999));
    assert_true(queue_size(q) == 7);

    assert_true(queue_iswrapped(q));

    for (int i = 2; i < QUEUE_START_CAP - 2; i++) {
        val = queue_dequeue(q);
        assert_true(*val == i);
    }
    for (int i = 0; i < 3; i++) {
        val = queue_dequeue(q);
        assert_true(*val == 997 + i);
    }

    assert_true(queue_size(q) == 0);

    queue_free(q);
}


// When queue_enqueue() is passed a NULL queue, a NULL error status is returned.
static void test_queue_enqueue_null(void **state)
{
    Queue *q = NULL;
    int *val = queue_enqueue(q, int_key(999));
    assert_true(val == NULL);
}

// When queue_dequeue() is passed a valid, empty queue, NULL is returned.
static void test_queue_dequeue_empty(void **state)
{
    Queue *q = queue_alloc(int_comp);
    int *val = queue_dequeue(q);
    assert_true(val == NULL);
    queue_free(q);
}

// When queue_dequeue() is passed a NULL queue, NULL is returned.
static void test_queue_dequeue_null(void **state)
{
    Queue *q = NULL;
    int *val = queue_dequeue(q);
    assert_true(val == NULL);
}

// When queue_peek() is passed a valid, populated queue, the next to be dequeued element is returned without being removed.
static void test_queue_peek(void **state)
{
    Queue *q = queue_alloc(int_comp);
    for (int i = 0; i < REALLOC; i++) {
        queue_enqueue(q, int_key(i));
    }

    for (int i = 0; i < REALLOC; i++) {
        int *val = queue_peek(q);
        assert_true(*val == i);
        assert_true(queue_size(q) == REALLOC - i);

        val = queue_dequeue(q);
        assert_true(*val == i);
    }
    queue_free(q);
}


// When queue_peek() is passed a valid, empty queue, NULL is returned.
static void test_queue_peek_empty(void **state)
{
    Queue *q = queue_alloc(int_comp);
    int *val = queue_peek(q);
    assert_true(queue_size(q) == 0);
    assert_true(val == NULL);
    queue_free(q);
}

// When queue_peek() is passed a NULL queue, NULL is returned.
static void test_queue_peek_null(void **state)
{
    Queue *q = NULL;
    int *val = queue_peek(q);
    assert_true(queue_size(q) == -1);
    assert_true(val == NULL);
}

int queue_tests(void)
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_queue_size),
        cmocka_unit_test(test_queue_size_empty),
        cmocka_unit_test(test_queue_size_empty_dequeue),
        cmocka_unit_test(test_queue_size_empty_dequeue_wrap),
        cmocka_unit_test(test_queue_size_null),
        cmocka_unit_test(test_queue_enqueue),
        cmocka_unit_test(test_queue_enqueue_wrap),
        cmocka_unit_test(test_queue_enqueue_wrap_realloc),
        cmocka_unit_test(test_queue_enqueue_wrap_end),
        cmocka_unit_test(test_queue_enqueue_null),
        cmocka_unit_test(test_queue_dequeue_empty),
        cmocka_unit_test(test_queue_dequeue_null),
        cmocka_unit_test(test_queue_peek),
        cmocka_unit_test(test_queue_peek_empty),
        cmocka_unit_test(test_queue_peek_null),
    };

    return cmocka_run_group_tests_name("queue", tests, NULL, NULL);
}

