#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <setjmp.h>
#include <cmocka.h>

#include <alibc/hashtbl.h>
#include <alibc/int.h>

#define MAX 8192

// When hashtbl_size() is passed a valid hashtbl, the correct size is returned.
static void test_hashtbl_size(void **state)
{
    HashTbl *h = hashtbl_alloc(int_comp);
    for (int i = 1; i <= MAX; i++) {
        hashtbl_put(h, int_key(i), int_key(i));
        assert_true(hashtbl_size(h) == i);
    }
    hashtbl_free(h);
}

// When hashtbl_size() is passed an empty hashtbl, 0 is returned.
static void test_hashtbl_size_empty(void **state)
{
    HashTbl *h = hashtbl_alloc(int_comp);
    assert_true(hashtbl_size(h) == 0);
    hashtbl_free(h);
}

// When hashtbl_size() is passed a NULL hashtbl, -1 is returned.
static void test_hashtbl_size_null(void **state)
{
    HashTbl *h = NULL;
    assert_true(hashtbl_size(h) == -1);
    hashtbl_free(h);
}

// When hashtbl_put() is passed a valid hashtbl, the values are placed into the hashtbl.
static void test_hashtbl_put(void **state)
{
    HashTbl *h = hashtbl_alloc(int_comp);
    for (int i = 1; i <= MAX; i++) {
        int *val = hashtbl_put(h, int_key(i), int_key(i));
        assert_true(hashtbl_size(h) == i);
        assert_true(val == NULL);
        val = hashtbl_get(h, int_key(i));
        assert_true(*val == i);
    }
    hashtbl_free(h);
}

// When hashtbl_put() is passed a NULL hashtbl, NULL is returned.
static void test_hashtbl_put_nulltbl(void **state)
{
    HashTbl *h = NULL;
    void *val = hashtbl_put(h, int_key(999), int_key(999));
    assert_true(val == NULL);
    assert_true(hashtbl_size(h) == -1);
}

// When hashtbl_put() is passed a valid hashtbl with a NULL key, no value is added and NULL is returned.
static void test_hashtbl_put_nullkey(void **state)
{
    HashTbl *h = hashtbl_alloc(int_comp);
    int *key = NULL;
    int *val = int_key(999);

    int *ret = hashtbl_put(h, key, val);

    assert_true(hashtbl_size(h) == 0);
    assert_true(ret == NULL);

    hashtbl_free(h);
}

// When hashtbl_put() is passed a valid hashtbl with a NULL value, no value is added and NULL is returned.
static void test_hashtbl_put_nullval(void **state)
{
    HashTbl *h = hashtbl_alloc(int_comp);
    int *val = NULL;
    int *key = int_key(999);

    int *ret = hashtbl_put(h, key, val);

    assert_true(hashtbl_size(h) == 0);
    assert_true(ret == NULL);

    hashtbl_free(h);
}

// When hashtbl_put() is passed a valid hashtbl with a key that already exists, the old value is returned and replaced.
static void test_hashtbl_put_replace_existing_key(void **state)
{
    HashTbl *h = hashtbl_alloc(int_comp);
    int *key1 = int_key(999);
    int *key2 = int_key(999);

    hashtbl_put(h, key1, key1);
    int *val = hashtbl_put(h, key2, key2);

    assert_true(hashtbl_size(h) == 1);
    assert_true(*val == 999);
    assert_true(val == key1);
    assert_true(val != key2);

    hashtbl_free(h);
}

// When hashtbl_get() is passed a valid hashtbl with a key that already exists, the value is returned.
static void test_hashtbl_get(void **state)
{
    HashTbl *h = hashtbl_alloc(int_comp);
    int *key = int_key(999);

    hashtbl_put(h, key, key);
    int *val = hashtbl_get(h, key);

    assert_true(hashtbl_size(h) == 1);
    assert_true(*key == *val);

    hashtbl_free(h);
}

// When hashtbl_get() is passed a valid hashtbl with a key that does not exist, NULL is returned.
static void test_hashtbl_get_noexist(void **state)
{
    HashTbl *h = hashtbl_alloc(int_comp);
    int *val = hashtbl_get(h, int_key(999));

    assert_true(val == NULL);

    hashtbl_free(h);
}

// When hashtbl_get() is passed a valid hashtbl with a NULL key, NULL is returned.
static void test_hashtbl_get_nullkey(void **state)
{
    HashTbl *h = hashtbl_alloc(int_comp);
    int *val = int_key(999);
    hashtbl_put(h, NULL, val);

    int *ret = hashtbl_get(h, NULL);
    assert_true(ret == NULL);

    hashtbl_free(h);
}

// When hashtbl_get() is passed a NULL hashtbl, NULL is returned.
static void test_hashtbl_get_nulltbl(void **state)
{
    HashTbl *h = NULL;
    int *val = hashtbl_get(h, int_key(999));
    assert_true(val == NULL);
}

// When hashtbl_remove() is passed a valid hashtbl with an existing key, the value is returned and the key is removed.
static void test_hashtbl_remove(void **state)
{
    HashTbl *h = hashtbl_alloc(int_comp);
    for (int i = 0; i < MAX; i++) {
        int *key = int_key(i);
        hashtbl_put(h, key, key);
    }

    int *val = hashtbl_remove(h, int_key(999));
    assert_true(*val == 999);
    assert_true(hashtbl_size(h) == MAX - 1);

    val = hashtbl_get(h, int_key(999));
    assert_true(val == NULL);

    val = hashtbl_remove(h, int_key(999));
    assert_true(val == NULL);
    assert_true(hashtbl_size(h) == MAX - 1);

    hashtbl_free(h);
}

// When hashtbl_remove() is passed a NULL hashtbl, NULL is returned.
static void test_hashtbl_remove_nulltbl(void **state)
{
    HashTbl *h = NULL;
    int *val = hashtbl_remove(h, int_key(999));
    assert_true(val == NULL);
}

// When hashtbl_remove() is passed a valid hashtbl with a NULL key, NULL is returned.
static void test_hashtbl_remove_nullkey(void **state)
{
    HashTbl *h = hashtbl_alloc(int_comp);
    int *val = hashtbl_remove(h, NULL);
    assert_true(val == NULL);
}

int hashtbl_tests(void)
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_hashtbl_size),
        cmocka_unit_test(test_hashtbl_size_empty),
        cmocka_unit_test(test_hashtbl_size_null),
        cmocka_unit_test(test_hashtbl_put),
        cmocka_unit_test(test_hashtbl_put_nulltbl),
        cmocka_unit_test(test_hashtbl_put_nullkey),
        cmocka_unit_test(test_hashtbl_put_nullval),
        cmocka_unit_test(test_hashtbl_put_replace_existing_key),
        cmocka_unit_test(test_hashtbl_get),
        cmocka_unit_test(test_hashtbl_get_noexist),
        cmocka_unit_test(test_hashtbl_get_nullkey),
        cmocka_unit_test(test_hashtbl_get_nulltbl),
        cmocka_unit_test(test_hashtbl_remove),
        cmocka_unit_test(test_hashtbl_remove_nulltbl),
        cmocka_unit_test(test_hashtbl_remove_nullkey),
    };

    return cmocka_run_group_tests_name("hashtbl", tests, NULL, NULL);
}

