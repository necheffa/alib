#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <setjmp.h>
#include <cmocka.h>

#include <alibc/stack.h>
#include <alibc/int.h>

#define MAX 8192

// A number to exceed internal vector reallocation.
static const int REALLOC = VECTOR_START_CAP + 4;

// When stack_size() is passed an empty stack, 0 is returned.
static void test_stack_size_empty(void **state)
{
    Stack *s = stack_alloc(int_comp);
    assert_true(stack_size(s) == 0);
    stack_free(s);
}

// When stack_size() is passed a NULL stack, -1 is returned.
static void test_stack_size_null(void **state)
{
    Stack *s = NULL;
    assert_true(stack_size(s) == -1);
    stack_free(s);
}

// When stack_size() is passed a valid, populated stack, the correct size is returned.
static void test_stack_size_normal(void **state)
{
    Stack *s = stack_alloc(int_comp);
    for (int i = 0; i < REALLOC; i++) {
        stack_push(s, int_key(i));
    }

    assert_true(stack_size(s) == REALLOC);
    stack_free(s);
}

// When stack_push() is called on a valid stack, values are added to the top of the stack.
static void test_stack_push(void **state)
{
    Stack *s = stack_alloc(int_comp);
    for (int i = 1; i <= REALLOC; i++) {
        int *k = int_key(i);
        stack_push(s, k);
        assert_true(stack_size(s) == i);

        int *l = stack_peek(s);
        assert_true(*k == *l);
    }
    stack_free(s);
}

// When stack_push() is called on a NULL stack, no action is taken.
static void test_stack_push_null(void **state)
{
    Stack *s = NULL;
    stack_push(s, int_key(999));
    assert_true(stack_size(s) == -1);
    stack_free(s);
}

// When stack_peek() is called on an empty stack, NULL is returned.
static void test_stack_peek_empty(void **state)
{
    Stack *s = stack_alloc(int_comp);
    assert_true(stack_peek(s) == NULL);
    stack_free(s);
}

// When stack_peek() is called on a NULL stack, NULL is returned.
static void test_stack_peek_null(void **state)
{
    Stack *s = NULL;
    assert_true(stack_peek(s) == NULL);
    stack_free(s);
}

// When stack_pop() is called on a valid, populated stack, the values are returned in reverse order.
static void test_stack_pop(void **state)
{
    Stack *s = stack_alloc(int_comp);
    for (int i = 1; i <= REALLOC; i++) {
        stack_push(s, int_key(i));
    }

    for (int i = REALLOC; i >= 1; i--) {
        int *k = stack_pop(s);
        assert_true(*k == i);
        assert_true(stack_size(s) == i - 1);
    }

    stack_free(s);
}

// When stack_pop() is called on an empty stack, NULL is returned.
static void test_stack_pop_empty(void **state)
{
    Stack *s = stack_alloc(int_comp);
    int *k = stack_pop(s);
    assert_true(k == NULL);
    stack_free(s);
}

// When stack_pop() is called on a NULL stack, NULL is returned.
static void test_stack_pop_null(void **state)
{
    Stack *s = NULL;
    int *k = stack_pop(s);
    assert_true(k == NULL);
    stack_free(s);
}

int stack_tests(void)
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_stack_size_empty),
        cmocka_unit_test(test_stack_size_null),
        cmocka_unit_test(test_stack_size_normal),
        cmocka_unit_test(test_stack_push),
        cmocka_unit_test(test_stack_push_null),
        cmocka_unit_test(test_stack_peek_empty),
        cmocka_unit_test(test_stack_peek_null),
        cmocka_unit_test(test_stack_pop),
        cmocka_unit_test(test_stack_pop_empty),
        cmocka_unit_test(test_stack_pop_null),
    };

    return cmocka_run_group_tests_name("stack", tests, NULL, NULL);
}

