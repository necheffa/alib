#include <stdio.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

int vector_tests(void);
int queue_tests(void);
int stack_tests(void);
int strbld_tests(void);
int rbtree_tests(void);
int hashtbl_tests(void);

int main(void)
{
    return vector_tests() |
           queue_tests() |
           stack_tests() |
           strbld_tests() |
           rbtree_tests() |
           hashtbl_tests();
}

