#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <setjmp.h>
#include <cmocka.h>

#include <alibc/rbtree.h>
#include <alibc/int.h>

#define MAX 8192

// When rbtree_size() is passed an empty rbtree, 0 is returned.
static void test_rbtree_size_empty(void **state)
{
    RbTree *t = rbtree_alloc(int_comp);
    assert_true(rbtree_size(t) == 0);
    rbtree_free(t);
}

// When rbtree_size() is passed a valid, populated rbtree, the correct size is returned.
static void test_rbtree_size(void **state)
{
    RbTree *t = rbtree_alloc(int_comp);

    for (int i = 1; i <= MAX; i++) {
        int *j = int_key(i);
        rbtree_insert(t, j);
        assert_true(rbtree_size(t) == i);
    }

    rbtree_free(t);
}

// When rbtree_size() is passed a NULL rbtree, -1 is returned.
static void test_rbtree_size_null(void **state)
{
    RbTree *t = NULL;
    assert_true(rbtree_size(t) == -1);
    rbtree_free(t);
}

// When rbtree_insert() is passed a valid rbtree, the values are placed into the rbtree.
static void test_rbtree_insert(void **state)
{
    RbTree *t = rbtree_alloc(int_comp);

    for (int i = 1; i <= MAX; i++) {
        int *j = int_key(i);
        int *val = rbtree_insert(t, j);
        assert_true(val == NULL);
    }

    rbtree_free(t);
}

// When rbtree_insert() is passed a NULL rbtree, NULL is returned.
static void test_rbtree_insert_nulltree(void **state)
{
    RbTree *t = NULL;
    void *val = rbtree_insert(t, int_key(999));
    assert_true(rbtree_size(t) == -1);
    assert_true(val == NULL);
}

// When rbtree_insert() is passed a valid rbtree with a NULL value, no value is added and NULL is returned.
static void test_rbtree_insert_nullvalue(void **state)
{
    RbTree *t = rbtree_alloc(int_comp);
    int *i = NULL;
    int *val = rbtree_insert(t, i);
    assert_true(rbtree_size(t) == 0);
    assert_true(val == NULL);
    rbtree_free(t);
}

// When rbtree_insert() is passed a valid rbtree with a value that already exists, the old value is returned and replaced.
static void test_rbtree_insert_existingvalue(void **state)
{
    RbTree *t = rbtree_alloc(int_comp);
    int *val1 = int_key(999);
    int *val2 = int_key(999);

    rbtree_insert(t, val1);
    int *val = rbtree_insert(t, val2);

    assert_true(rbtree_size(t) == 1);
    assert_true(*val == *val1);
    assert_true(val == val1);
    assert_true(val != val2);

    rbtree_free(t);
}

// When rbtree_breadth_first() is passed a valid, populated rbtree, a breadth-first search is performed.
static void test_rbtree_breadth_first(void **state)
{
    RbTree *t = rbtree_alloc(int_comp);
    int vals[] = { 10, 5, 15, 2, 8, 12, 18 };
    int8_t len = sizeof(vals)/sizeof(vals[0]);

    for (int i = 0; i < len; i++) {
        rbtree_insert(t, int_key(vals[i]));
    }

    int expected[] = { 10, 5, 15, 2, 8, 12, 18 };
    Vector *v = rbtree_breadth_first(t);
    assert_true(vector_size(v) == len);
    assert_true(vector_size(v) == rbtree_size(t));
    for (int i = 0; i < len; i++) {
        int *val = vector_get(v, i);
        assert_true(*val == expected[i]);
    }

    vector_free(v);
    rbtree_free(t);
}

// When rbtree_breadth_first() is passed a valid, empty rbtree, NULL is returned.
static void test_rbtree_breadth_first_empty(void **state)
{
    RbTree *t = rbtree_alloc(int_comp);
    Vector *v = rbtree_breadth_first(t);
    assert_true(v == NULL);
    assert_true(rbtree_size(t) == 0);

    rbtree_free(t);
}

// When rbtree_breadth_first() is passed a NULL rbtree, NULL is returned.
static void test_rbtree_breadth_first_null(void **state)
{
    RbTree *t = NULL;
    Vector *v = rbtree_breadth_first(t);
    assert_true(t == NULL);
    assert_true(v == NULL);
}

// When rbtree_depth_first() is passed a valid, populated rbtree, a depth-first search is performed.
static void test_rbtree_depth_first(void **state)
{
    RbTree *t = rbtree_alloc(int_comp);
    int vals[] = { 10, 5, 15, 2, 8, 12, 18 };
    int8_t len = sizeof(vals)/sizeof(vals[0]);

    for (int i = 0; i < len; i++) {
        rbtree_insert(t, int_key(vals[i]));
    }

    int expected[] = { 10, 5, 2, 8, 15, 12, 18 };
    Vector *v = rbtree_depth_first(t);
    assert_true(vector_size(v) == len);
    assert_true(vector_size(v) == rbtree_size(t));
    for (int i = 0; i < len; i++) {
        int *val = vector_get(v, i);
        assert_true(*val == expected[i]);
    }

    vector_free(v);
    rbtree_free(t);
}

// When rbtree_depth_first() is passed a valid, empty rbtree, NULL is returned.
static void test_rbtree_depth_first_empty(void **state)
{
    RbTree *t = rbtree_alloc(int_comp);
    Vector *v = rbtree_depth_first(t);
    assert_true(v == NULL);
    assert_true(rbtree_size(t) == 0);

    rbtree_free(t);
}

// When rbtree_depth_first() is passed a NULL rbtree, NULL is returned.
static void test_rbtree_depth_first_null(void **state)
{
    RbTree *t = NULL;
    Vector *v = rbtree_depth_first(t);
    assert_true(t == NULL);
    assert_true(v == NULL);
}

int rbtree_tests(void)
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_rbtree_size_empty),
        cmocka_unit_test(test_rbtree_size),
        cmocka_unit_test(test_rbtree_size_null),
        cmocka_unit_test(test_rbtree_insert),
        cmocka_unit_test(test_rbtree_insert_nulltree),
        cmocka_unit_test(test_rbtree_insert_nullvalue),
        cmocka_unit_test(test_rbtree_insert_existingvalue),
        cmocka_unit_test(test_rbtree_breadth_first),
        cmocka_unit_test(test_rbtree_breadth_first_empty),
        cmocka_unit_test(test_rbtree_breadth_first_null),
        cmocka_unit_test(test_rbtree_depth_first),
        cmocka_unit_test(test_rbtree_depth_first_empty),
        cmocka_unit_test(test_rbtree_depth_first_null),
    };

    return cmocka_run_group_tests_name("rbtree", tests, NULL, NULL);
}

