// Copyright (C) 2016, 2023, 2024 Alexander Necheff
// This program is licensed under the terms of the LGPLv3.
// See the COPYING and COPYING.LESSER files that came packaged with this source code for the full terms.

#include <stdint.h>
#include <stdlib.h>

#include <alibc/stack.h>

Stack *stack_alloc(int32_t (*comp)(void *, void *))
{
    Stack *s = vector_alloc(comp);
    return s;
}

void stack_free(Stack *s)
{
    vector_free(s);
}

void stack_push(Stack *s, void *data)
{
    vector_put(s, data);
}

void *stack_peek(Stack *s)
{
    if (!s || stack_size(s)  - 1 < 0) {
        return NULL;
    }

    return vector_get(s, stack_size(s) - 1);
}

void *stack_pop(Stack *s)
{
    if (!s || stack_size(s) - 1 < 0) {
        return NULL;
    }

    return vector_remove(s, stack_size(s) - 1);
}

int64_t stack_size(Stack *s)
{
    return vector_size(s);
}

