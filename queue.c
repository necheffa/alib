// Copyright (C) 2017, 2023, 2024 Alexander Necheff
// This program is licensed under the terms of the LGPLv3.
// See the COPYING and COPYING.LESSER files that came packaged with this source code for the full terms.

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <alibc/queue.h>

/*
 * Doubles the capacity of the specified Queue.
 */
static void queue_resize(Queue *);

Queue *queue_alloc(int32_t (*comp)(void *, void *))
{
    Queue *q = calloc(1, sizeof(*q));

    q->max = QUEUE_START_CAP;
    q->data = calloc(q->max, sizeof(void *));
    q->start = 0;
    q->end = 0;
    q->comp = comp;

    return q;
}

void queue_free(Queue *q)
{
    if (q) {
        free(q->data);
        free(q);
    }
}

void *queue_enqueue(Queue *q, void *data)
{
    if (!q) {
        return NULL;
    }

    if (queue_size(q) >= q->max - 1) {
        // we resize at a size of q->max - 1 so that we can always tell the
        // difference between an empty (q->start == q->end) and a "full" queue.
        // we trade one wasted space for not having an increment/decrement every enqueue/dequeue
        // to track size explicitly in a seporate q->len member.
        queue_resize(q);
    }

    q->data[q->end] = data;

    if (q->end + 1 >= q->max) {
        q->end = 0;
    } else {
        q->end++;
    }

    return data;
}

void *queue_dequeue(Queue *q)
{
    if (!q || queue_size(q) == 0) {
        return NULL;
    }

    void *data = q->data[q->start];

    if (q->start + 1 >= q->max) {
        q->start = 0;
    } else {
        q->start++;
    }

    return data;
}


void *queue_peek(Queue *q)
{
    if (!q || queue_size(q) == 0) {
        return NULL;
    }

    return q->data[q->start];
}

int64_t queue_size(Queue *q)
{
    if (q) {
        if (q->start <= q->end) {
            return q->end - q->start;
        } else {
            return q->max - q->start + q->end;
        }
    }
    return -1;
}

static void queue_resize(Queue *q)
{
    void **old = q->data;
    void **new = calloc(q->max * 2, sizeof(void *));

    if (q->end > q->start) {
        // we are not wrapped, so just copy the whole thing in one go.
        memcpy(new, old, (q->max - 1) * sizeof(void *));
    } else {
        // we are wrapped, do the copy in parts and so we unwrap too.
        memcpy(new, old + q->start, (q->max - q->start) * sizeof(void *));
        memcpy(new + (q->max - q->start), old, (q->end + 1) * sizeof(void *));
    }
    q->end = q->max - 1;
    q->start = 0;
    q->data = new;
    q->max = q->max * 2;

    free(old);
}
