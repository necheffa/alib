CC=/usr/bin/gcc
#CC=/usr/bin/clang
AR=/usr/bin/gcc-ar
#AR=/usr/bin/ar
LD=/usr/bin/ld
SRP=/usr/bin/strip
CFLAGS=-Wall -Wextra -std=c17 -pedantic -fPIC -march=x86-64-v2
SEC=-fstack-protector-strong -fstack-clash-protection -fcf-protection=full -ftrivial-auto-var-init=pattern
SANITIZE=-fsanitize=undefined -fsanitize=address -fsanitize=leak $(SEC)
DEBUG=-ggdb -fno-omit-frame-pointer -O0
PROD=-O3 -flto $(SEC)
CPPFLAGS=-U_FORTIFY_SOURCE -D_FORTIFY_SOURCE=3 -U_GLIBCXX_ASSERTIONS -D_GLIBCXX_ASSERTIONS=1
LIBSEARCH=-Iinclude/
CCLDFLAGS=-Wl,-z,relro,-z,now -fPIC $(LIBSEARCH)
# turn off optimization during debug

VERSION_ABI:=$(shell cat VERSION_ABI)
MAJOR:=$(shell awk -F. '{print $$1}' < VERSION_ABI)
BASENAME:=libalibc.so

DYNLIB:=$(BASENAME).$(MAJOR)

LIBS=libalibc.a \
     $(DYNLIB)

TESTOBJS=vector_test.o \
		 stack_test.o \
		 queue_test.o \
		 strbld_test.o \
		 rbtree_test.o \
		 hashtbl_test.o

OBJS=strbld.o \
	 vector.o \
	 hashtbl.o \
	 rbtree.o \
	 stack.o \
	 queue.o \
	 version.o

# Always do production builds by default.
# all optimizations on, some code generation features like stack protector
prod: CFLAGS += $(PROD)
prod: CFLAGS += $(CPPFLAGS)
prod: all

# general build of alibc
all: $(LIBS) int.o

debian: prod
	VERSION_ABI=$(VERSION_ABI) MAJOR=$(MAJOR) BASENAME=$(BASENAME) scripts/package-deb

quality:
	cppcheck --enable=all --force --quiet -I include/ *.c tests/*.c

# use with gcov
coverage: CFLAGS += --coverage
coverage: debug

# an even more special case of debug with extra code generation
# and all optimizations on - intended to give a second opinion for
# running tests; gdb users beware lest "the variable was optimized away"
maxdebug: CFLAGS += $(SANITIZE)
maxdebug: CFLAGS += $(CPPFLAGS)
maxdebug: CFLAGS += -ggdb -fno-omit-frame-pointer -O3 -flto
maxdebug: all unittests

# special case of debug; includes extra code generation features
# that might otherwise obscure the debugger - intended for running tests
sanitize: CFLAGS += $(SANITIZE) $(DEBUG)
sanitize: CFLAGS += $(CPPFLAGS) $(PROD)
sanitize: all unittests

# a simple debug target with limited code generation opttions, all optimizations off,
# and extra debugging symbols - intended for interactive debugging with gdb and friends
debug: CFLAGS += $(DEBUG)
debug: all unittests

version.o: version.inc

version.inc:
	scripts/version

unittests: CFLAGS += -Wno-unused-parameter $(SEC) $(CPPFLAGS)
unittests: libalibc.a $(TESTOBJS) int.o
	$(CC) $(CFLAGS) $(CCLDFLAGS) -o unittests tests/testdriver.c $(TESTOBJS) libalibc.a int.o -lcmocka
	./unittests 2>&1 | tee test.log && echo "All tests complete, results located in test.log"

test: unittests

version: $(OBJS) version.inc
	$(CC) $(CFLAGS) $(CCLDFALGS) -o version version.o

$(DYNLIB): $(OBJS) version.inc
	$(CC) -shared -Wl,-soname,$(DYNLIB) -o $(DYNLIB) $(OBJS)

libalibc.a: $(OBJS) version.inc
	$(AR) -r libalibc.a $(OBJS)

vpath %_test.c tests/

%.o: %.c
	$(CC) $(CFLAGS) $(LIBSEARCH) -c $<

clean:
	rm -f $(OBJS) $(LIBS) int.o *.deb test.log *.gcno *.gcda *.gcov version.inc unittests $(TESTOBJS)
