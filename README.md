# alib

alib provides a somewhat generic data structures and utilities library for C programs.

## Getting Started

alib requires a Unix-like system to build. See the provided Makefile for details.

### Status

Currently `HashTbl`, `Vector`, `Queue`, `Stack`, and `StrBld` are considered stable. The `RbTree` is under development.

### Compilation

For general use the `prod` target should be used, e.g. `make prod`, as this will enable all optimizations and strip binaries. This is also the default build target if none is specified.

Additional targets intended to help testing and debugging include `debug`, `sanitize`, and `maxdebug`. Of course, see the Makefile for
all provided targets.

We do most of our development work with gcc, and to a lesser extent clang. But any modern standards compliant C compiler should work, you may
need to tweek the Makefile, particularlly for the formatting of compiler arguments.

### Test Execution

alib includes a suite of unit tests, cmocka is a required dependency of the test suite although it may be safely ignored if you only care about building the production library.

Most of the existing diagnostic build targets will also build and execute the unit tests. Although the generated `unittests` binary may be manually executed if desired.
Automatic test execution will report to STDOUT and `test.log` for review.

### Documentation

Currently there is a somewhat out of date "code conventions" document located at `doc/conventions.txt`.

The API is also documented in source code comments. Although some functions are document in both the .c and .h files, eventually only the .h files
will contain API documentation.

## Design Philosophy

alib tries to be C standard compliant. As of 1.4.0, the default Makefile will require C17 for syntax checking but one could probably get
away with a C99 compiler. alib generally tries to avoid C89 conventions when a >=C99 language construct or library call is available. This
implies that alib aims to be modern in its design choices.

Prior to 1.4.0, alib took a laissez-faire stance when it came to error checking at the API. This resulted in a number of subtle bugs,
NULL pointer dereferences, and even one or two double-free senarios. Starting with 1.4.0, alib attempts to strengthen error checking without
being too overbearing.

In many instances, alib favors readability to performance. The reality is that someone who cares about performance is just going to use C++
so that they can access the collections library contained within libstdc++. This does not mean alib should be slow, it just means we aren't going
to write a tangled web purely for performance reasons.

alib generally tries to follow semantic versioning and thus maintain API/ABI compatability. This can be hard and sometimes mistakes are made.

The 1.x branch of alib exposed struct definitions for each of the container types, potentially allowing consumers to access the internal
implementation details. Starting with 1.4.0, this functionality is deprecated with a planned removal at version 2.0.0. 2.0.0 will usher in
completely opaque pointers for all container types in order to make ABI compatability much easier to maintain.

## Licensing

Starting with 1.4.0, alib is released under the terms of the LGPLv3, a copy of the license is provided in the
COPYING and COPYING.LESSER files located at the root of this repo.

Prior to 1.4.0, alib was released under the terms of the GPLv2, and a copy of that license was provided in
the LICENSE file at the root of the repo when referencing old releases.
