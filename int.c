// Copyright (C) 2016, 2023, 2024 Alexander Necheff
// This program is licensed under the terms of the LGPLv3.
// See the COPYING and COPYING.LESSER files that came packaged with this source code for the full terms.

#include <string.h>
#include <stdint.h>
#include <stdlib.h>

#include <alibc/int.h>

int32_t int_comp(void *a, void *b)
{
    int x = *((int *)a);
    int y = *((int *)b);

    if (x > y) {
        return 1;
    } else if (x < y) {
        return -1;
    }

    return 0;
}

int *int_key(int prim)
{
    int *k = malloc(sizeof(*k));
    memcpy(k, &prim, sizeof(prim));

    return k;
}
