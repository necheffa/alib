CONVENTIONS and STYLE:
The following conventions are used throughout this library.
Consider them to be an additional "interface".

- All data structures will contain a function pointer `comp` that
is passed as an argument to and assigned in the corrisponding `alloc` function.
`comp` will use the following prototype: `int32_t (*comp)(void *, void *);`
Assume the left-most argument as 'a' and the right-most as 'b';
if a > b return < 0
if a == b return 0
if a < b return > 0
This definition is flexable enough to allow for both simple comparisons and more
sofisticated "lexical ordering".
The level of sofistication required depends on and is documented in each individual
data structure implementation. This allows consumers to establish a library of
reuseable comparison functions that do not need to be tailor made for use with `alibc`.
Future versions of `alibc` will hopefully provide a standard set of comparison functions.
Data structures making use of `comp` internally assume the implementation of `comp` adhears
to the above interface.

- All index variables being passed in to a function use a 32 bit unsigned integer.
However, return values, for example from an `indexof` function, use a 64 bit signed integer.
Use of a 32 bit unsigned integer guarentees the specified index is never < 0 and therefore
makes internal validation simpler as the library need only check if the index exceeds the
allocated space in the data structure being indexed.
When functions like `indexof` return 64 bit signed integers, they allow for the function to
return an index in the range of the 32 bit unsigned integer space as well as '-1' to indicate
a failure to locate the element being asked about.

- Use of the preprocessor shall be limited to header includes, header guards, and define macros
for named constants (e.g. #define BUF_MAX 8192).
All other preprocessor usage, even simple macro functions is strictly forbidden. Compiler optimizations
and hardware developments mean that it is no longer nessicary to sacrifice readability for
what is now marginal, although historically has been signficant, performance gains.

- Each "module" is "namespaced" by appending the module name followed by an underscore followed by
the specific function. For example the `alloc` function of the vector module is `vector_alloc()`.
Camel case is strictly forbidden. This keeps function names predictable.

- An exerpt from the Linux coding style guide says "C is a Spartan language, and so should your naming be."
This is a hard rule. If you find yourself needing long discriptive names for variables it is a
large red flag that your code is bad and you should feel bad. When functions are short and therefore have
few variables in the scope of execution - there is no need for anything more than short but sweet variable names.
Idiomatic C often dictates 'c' for char, 'i' for int, and 'p' for a pointer. If your for loop control variable
is anything besides 'i' you are doing it very wrong.

- Bracing is strictly K&R. That is, the function opening brace is on the following line by itself and
statement braces end the line that the statement started on.
Each closing brace is on a line by itself, naturally, with the exception of the do-while loop where
it is expected that the while condition imediately follows the closing brace of the do block.
Statements should _always_ use braces. For example, an if statement with a single statement true block
will have that true block enclosed in braces.

- Indentation is 4 ASCII space characters per level; no more, no less, and certainly not '\t'.
On the subject of indentation, code indented more than 3 levels (not including the function block) is
another code quality red flag. Understandbly there are some times more than 3 levels are needed but
if you find yourself leaning on this more than once in a great while you need to consider a redesign.

- This library is written in C, specifically C99. Not COBOL nor Fortran, but C. We don't have
punch card limitations superimposed on digital files like program divisions. Keep this in mind when
declaring variables; they don't all need to go to the top of the function.
As a general rule, keep variable declaraions at the highest point of their scope. So function wide variables
should be put at the top of the function but a variable only used in the body of a while loop can be declaired
either in the body of the while loop or just before the while loop statement depending on the life time desired.
This keeps code clean and organized and limits the number of times you need to seek back up to inspect a variable
declaration.
As this is C, definitions seporate from declaraions, if ever so slightly. Keep definitions as close to declarations
as possible.

- Broadly, there are two kinds of comments; "code" and "documentation". Documentation comments
describe the behavior and paramaters of a function and should appear in the header of a module
above each function prototype. They are to use C-style multi-line comments; that is:
/* comment
 * text
 */
and not Javadoc style:
/**
 * comment text
 */
Nor shall they use C++ style, single line comments.
It is expected that each function prototype in a header will include some documentation.
The function definitions in the translation unit should not have documentation comments as
they would simply be duplicates of the header prototypes.
On the other hand, code comments are generally frowned upon. Code should be self-documenting. If you
feel someone needs to read your comment to understand the code then as a general rule your code is bad.
A good code comment describes _why_ something was done (as opposed to another, perhaps more obvious approch).
In general, good code has few to no comments because good code doesn't need to be commented to understand
what it is expressing. Even if you are implementing a complex algorithm, perhaps mention you are
implementing _that_ algorithm but otherwise assume the reader is familiar.
It is the readers' onus to understand the theory behind the code _before_ reading.
For example, a reader not familiar with weighted graph traversal should not expect code comments
to bring them up to spead on Dijkstra's shortest-path first or Prim's minimum spaning tree algorithms.
As a final note - just as C naming is Spartan, so is C commenting.

- Who frees the mallocs? In `alibc`, it depends. Just as each data structure includes an `alloc` function,
it shall also provide a `free` function. Any memory allocated by the library is freed by the library. Careful
inspection of the data structures will show consumer data is simply a void pointer tracked by the data structure.
In this case, the memory is allocated by the consumer and so it is the consumer's responsibility to free the memory.
The rational is thus: consider the `HashTbl`, a consumer places data in the `HashTbl` and later removes the data.
If `HashTbl` freed the memory upon removal then the consumer would receave an invalid pointer instead of their data.
Even if it were not the case, the `HashTbl` has no way of knowing how long the life span of the data will be, it could
be that the `HashTbl` is just a transient container for data with a life span outliving even the `HashTbl` itself.
On the otherhand, each data structure shall provide some mechanism for iterating over each element (traversal order
does not need to be guarenteed) to allow a consumer to simply free the user data before freeing the data structure.
Any data structure not providing such a mechanism shall be considered to have a defect.

- In general, all memory groupings such as structs, unions, and bit-fields should be
provided as an opaque typedef. With a well designed module, a consumer does not (and should not)
need to be aware of how exactly a data structure is implemented. The function prototypes declaired
in the headers are the public interface to each module, implementations are free to redefine
module internals provided they preserve the functionality expressed by and documented in the headers.
Typedefs, however, may never be used to rename primitive types such as float or int nor shall they
be used to hide pointers to memory groupings.
Given the following struct:
struct MyStruct {
    int i;
};
The typedef should take the following form:
typedef struct MyStruct MyStruct;
Notice the name of the struct and typedef match and are capitalized; this is another rule.
Never prefix or postfix memory grouping names with underscores.
Camel case is accepted and encouraged for naming memory groupings to provide contrast with
the rest of the code.
The same convenions apply equally to unions and bit-fields.

- In a perfect world, testing would stress each branch in each function with each edge case. In practice, this
is a nearly impossible task. Tests should not be artificially kept limited, they should be as exaustive as possible.
Tests should not rely on humans reading output or scripts diffing output to confirm validation.
In particular, each unit test should be written so that `assert()` is used to assert valid behavior. The last a unit test
should do before returning is print a message to STDOUT indicating that all tests executed to completion.
In otherwords, I should be able to run unit tests and upon success get an "all tests completed" message, otherwise
a nice stack trace from `assert()`.
